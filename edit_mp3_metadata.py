#!/usr/bin/python2.7

import mutagen
from mutagen.easyid3 import EasyID3
import os

root_path = "/home/xmars/English/Listening_Practice_Through_Dictation"

for path, subdirs, files in os.walk(root_path):
    # read all mp3 files exist in the root directory
    for name in files:
        file_path = os.path.join(path, name)
        file_name = name.replace('.mp3', '').strip() + ' '
        try:
            part_number = file_name[file_name.find('_') + 1]
            file_metadata = EasyID3(file_path)
            album = ['LPTD_' + part_number]
            tracknumber = file_metadata['tracknumber'][0] if file_metadata['tracknumber'] else ''
            if len(tracknumber) == 1:
                tracknumber = '0'+tracknumber
            file_metadata['album'] = album
            file_metadata['artist'] = album

            file_metadata['title'] = ['Track '+ part_number + tracknumber]
            file_metadata.save()
            print(file_metadata)
            # file_metadata.save()
        except Exception:
            pass
