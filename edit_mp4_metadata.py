#!/usr/bin/python2.7
# first line point to whatever environment is use for this file
from mutagen.mp4 import MP4

import os

root_path = '/home/xmars/English/MasterSpokenEnglish-FeelingPhonics'

for path, subdirs, files in os.walk(root_path):
    # read all MP4 files exist in the root directory
    for name in files:
        file_path = os.path.join(path, name)
        file_name = name.replace('.mp4', '').strip() + ' '
        try:
            file_metadata = MP4(file_path)
            metadata_nam = ''.join(file_metadata['\xa9nam'])
            metadata_ART = ''.join(file_metadata['\xa9ART'])
            if file_name in metadata_nam :
                metadata_nam = metadata_nam.replace(file_name, '')
            if file_name in metadata_ART:
                metadata_ART = metadata_ART.replace(file_name, '')
            file_metadata['\xa9nam'] = file_name + metadata_nam
            # file_metadata['\xa9ART'] = file_name + metadata_ART
            file_metadata['\xa9ART'] = metadata_ART
            file_metadata.save()
        except Exception:
            pass

